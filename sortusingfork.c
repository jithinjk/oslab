#include <stdio.h>   /* printf, stderr, fprintf */
#include <sys/types.h> /* pid_t */
#include <unistd.h>  /* _exit, fork */
#include <stdlib.h>  /* exit */
#include <errno.h>   /* errno */
 
int a[20], n;
void ascending()
{
   int i, j, t;
   for (i = 0; i < n; i++)
   {
       for (j=i + 1; j < n; j++)
          {
              if (a[i] > a[j])
                 {
                     t = a[i];
                     a[i] = a[j];
                     a[j] = t;
                 }
            }
    }
   printf("\nSorted array(Ascending): ");
   for (i = 0; i < n; i++)
      printf("%d\t ",a[i]);


}

void descending()
{

    int i, j, t;
    for (i = 0; i < n; i++)
    {
       for (j=i + 1; j < n; j++)
           {
              if (a[i] < a[j])
                 {
                     t = a[i];
                     a[i] = a[j];
                     a[j] = t;
                 }
            }
     }
    printf("\nSorted array(Desending): ");
    for (i = 0; i < n; i++)
  	printf("%d\t ",a[i]);
}


int main(void)
{
   int  pid;
   int i,j;
   printf("Enter limit : "); 
   scanf("%d",&n);
   printf("Enter the no's :\n");
   for(i = 0;i<n;i++)
	scanf("%d",&a[i]);

   /* Output from both the child and the parent process
    * will be written to the standard output,
    * as they both run at the same time.
    */
   pid = fork();
   if (pid == -1)
   {   
      /* Error:
       * When fork() returns -1, an error happened
       * (for example, number of processes reached the limit).
       */
      fprintf(stderr, "can't fork, error %d\n", errno);
      exit(EXIT_FAILURE);
   }
 
   if (pid == 0)
   {
      /* Child process:
       * When fork() returns 0, we are in
       * the child process.
       */
   	printf("Child process....");
	printf("\nProcessID : %d,Parent ID : %d",getpid(),getppid());
	ascending();
	printf("\n");
   }
   else
   { 
 
      /* When fork() returns a positive number, we are in the parent process
       * (the fork return value is the PID of the newly created child process)
       */
	printf("Parent process....");
        printf("\nProcessID : %d,Parent ID : %d",getpid(),getppid());
        descending();
        printf("\n");
     }
   return 0;
}
